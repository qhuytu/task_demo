let taskList = [];

function printTaskList(container, data) {
    let table = $("<table/>").addClass("CSSTableGenerator");
    let header = $("<tr/>");
    header.append($("<th/>").text("Name"));
    header.append($("<th/>").text("Done"));
    table.append(header);
    data.forEach(element => {
        let row = $("<tr/>");
        for (let value in element) {
            row.append($("<td/>").text(value));
        }
        table.append(row);
    });
    container.append(table);
    return container;
}

function addTask(table, rowData) {
    let lastRow = $("<tr/>").appendTo(table.find("table:last"));
    for (let index in rowData) {
        if (index === 'finish') {
            let checkBox = $('<input/>').attr('type', 'checkbox');
            if (rowData[index]) {
                //finsh
            }
            lastRow.append(checkBox);
        } else {
            lastRow.append($("<td/>").text(rowData[index]));
        }
    }
    return lastRow;
}

$(document).ready(function () {
    var tableTask = printTaskList($("#taskList"), taskList);

    $("#addTask").click(function () {
        if ($("#taskName").val() !== "") {
            let newTask = {
                name: $("#taskName").val(),
                finish: false
            };
            taskList.push(newTask);
            addTask(tableTask, newTask);
            $("#taskName").val("");
        }
    });
});
